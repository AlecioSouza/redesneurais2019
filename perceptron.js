/**
 * @author Alécio Gomes de Souza
 * Perceptron simples.
 */

class Perceptron {

    constructor(inputs,weights,n,bias,biasw){
        this.inputs = inputs;
        this.weights = weights;
        this.n = n;
        this.bias = bias;
        this.biasw = biasw;
    }

    /**
     * Treina o perceptron
     * @param {Array} inputs Dados de entrada
     * @param {number} d Resultado de saída correto
     */
    train(inputs,d){
        this.inputs = inputs;
        let weights = this.weights;
        
        //Adiciona os dados do bias às entradas e pesos
        inputs.push(this.bias);
        weights.push(this.biasw);

        console.log("Inputs: ",inputs);
        console.log("Weights: ",weights);

        //Multiplica as entradas pelos pesos
        let e = this.multiply(inputs,weights);
        //Faz a soma dos valores do array
        let sum = this.sum(e);
        //Aplica a regra degrau
        let y = this.step(sum);

        console.log("Multiplicacao: ", e, "Soma: " + sum);

        //Verifica se o resultado acertou ou erro
        if(y === d){
            console.log("Acerto:" +  y);
        }else{
            console.log("Erro:" +  y);
            //Corrige os pesos
            this.correct(weights,d,y);
            console.log("Pesos corrigidos:", this.weights);
        }
    }

    /**
     * Aplica a regra do degrau
     * @param {number} x resultado da soma
     * @returns {number} resultado arredondado para 0 ou 1
     */
    step(x){
        return (x > 0) ? 1 : 0;
    }

    /**
     * Corrige os pesos
     * @param {Array} weights Pesos antigos 
     * @param {number} d Resultado esperado 
     * @param {number} y Resultado obtido
     */
    correct(weights,d,y){
        weights = weights.map((w,i) =>{
            return w + (this.n * (d - y) * this.inputs[i]);
        }).filter(w => { return !Number.isNaN(w) });

        this.biasw = weights.splice(-1,1)[0];
        this.weights = weights;
    }

    /**
     * Soma os elementos do array
     * @param {Array} x Array
     * @returns {number} resultado da soma
     */
    sum(x){
        let sum = 0;
        x.forEach(z => { sum += z });
        return sum;
    }
    
    /**
     * Multiplica o array x pelo y
     * @param {Array} x 
     * @param {Array} y 
     * @returns {Array} resultado da multiplicação
     */
    multiply(x,y){ return x.map((z,i) => {return z * y[i] }); }
}

class Treinador {

    constructor(inputs,weights,results,n,bias,biasw){
        this.inputs = inputs;
        this.weights = weights;
        this.results = results;
        this.n = n;
        this.bias = bias;
        this.biasw = biasw;
        this.perceptron = new Perceptron([],this.weights,this.n,this.bias,this.biasw);
        this.epoca = 1;
    }

    /**
     * Treina o perceptron com os dados de entrada, pesos e resultados informados
     */
    execute(){
        this.inputs.forEach((input,i) =>{
            console.log("EPOCA - " + this.epoca);
            this.perceptron.train(input,this.results[i]);
            this.epoca++;
            console.log("------------------------------------------------------------------------------------")
        });
    }


}

treinador = new Treinador(
    [[1,1], //Entradas
     [0.5, 0],
     [0.6, 0],
     [0.8, 1],
     [0.9, 0]],
    [0.7,-0.1], //Pesos
    [1,0,0,1,1], //Resultados
    0.3, //Fator de aprendizagem
    -1, //Bias
    0.2 //Peso do bias
);


treinador.execute();